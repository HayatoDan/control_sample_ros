#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import numpy as np
from geometry_msgs.msg import Twist, Pose, PoseStamped, Point
import tf

class Timer(object):
    def __init__(self,now):
        self.lasttime = now.to_sec()
    
    def calcDt(self,now):
        nowsec = now.to_sec()
        dt = (nowsec - self.lasttime)
        self.lasttime = nowsec
        return dt

class RigidBody(object):
    def __init__(self):
        rospy.init_node('rigid_body', disable_signals=True)

        # set subscriber and publisher
        self.vel_sub = rospy.Subscriber('cmd_vel', Twist, self.velCallback)
        self.pose_pub = rospy.Publisher('pose', Pose, queue_size=1)
        # set tf listner and broadcaster
        self.listener = tf.TransformListener()
        self.broadcaster = tf.TransformBroadcaster()

        # set clock
        clock = rospy.get_param("~clock",60)
        self.rate = rospy.Rate(clock)

        # Get tfname published to
        self.tfname = rospy.get_param('~name_space', default='rigid_body')

        # set initial position
        self.x = rospy.get_param("~x", default="0.0")
        self.y = rospy.get_param("~y", default="0.0")
        self.z = rospy.get_param("~z", default="0.0")

        # set initial body velocity
        self.body_linear_vel = np.array([0, 0, 0])
        self.body_angular_vel = np.array([0, 0, 0])
        

        # Initialize rigid body
        ginit = [self.x, self.y, self.z]
        self.g = np.eye(4)
        self.g[0:3,3:4] = np.c_[np.array(ginit)]
        self.gdot = self.gdot_old = np.zeros((4, 4))

        # prepare pose message to publish
        self.pose = Pose()

        # initialize timer
        self.timer = Timer(rospy.Time.now())

    def velCallback(self,msg):
        self.body_linear_vel = np.array([msg.linear.x, msg.linear.y, msg.linear.z])
        self.body_angular_vel = np.array([msg.angular.x, msg.angular.y, msg.angular.z])

    def wedge(self,u):
        u_wedge = np.array([[0, -u[2], u[1]],
                            [u[2], 0, -u[0]],
                            [-u[1], u[0], 0]])
        return u_wedge

    def vel_wedge(self,v, w):
        V_wedge = np.array([[0,-w[2], w[1], v[0]],
                            [w[2], 0, -w[0], v[1]],
                            [-w[1], w[0], 0, v[2]],
                            [0, 0, 0, 0]])
        return V_wedge

    def spin(self):
        while not rospy.is_shutdown():
            # get samplingtime
            now = rospy.Time.now()
            dt = self.timer.calcDt(now)

            # Caluculate next pose of rigid body
            Vb = self.vel_wedge(self.body_linear_vel, self.body_angular_vel)
            self.gdot = np.dot(self.g, Vb)
            # 台数積分
            self.g += (self.gdot + self.gdot_old) * dt / 2
            self.gdot_old = self.gdot

            # # Projection of rotation matrix to SO(3)
            rotations = tf.transformations.rotation_from_matrix(self.g)
            projected_R = tf.transformations.rotation_matrix(rotations[0], rotations[1], rotations[2])
            self.g[0:3, 0:3] = projected_R[0:3, 0:3]

            position = self.g[0:3, 3:4].T.tolist()[0]
            orientation = tf.transformations.quaternion_from_matrix(projected_R)

            # Broadcast tf
            self.broadcaster.sendTransform(position, orientation, now, self.tfname, '/world')

            # Publish PoseStamped to topic
            self.pose.position.x = position[0]
            self.pose.position.y = position[1]
            self.pose.position.z = position[2]
            self.pose.orientation.x = orientation[0]
            self.pose.orientation.y = orientation[1]
            self.pose.orientation.z = orientation[2]
            self.pose.orientation.w = orientation[3]
            self.pose_pub.publish(self.pose)

            self.rate.sleep()

if __name__ == '__main__':
    try:
        rb = RigidBody()
        rb.spin()
    except rospy.ROSInterruptException: pass
