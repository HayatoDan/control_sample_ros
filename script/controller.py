#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist, Pose, PoseStamped, Point

class Controller(object):
	def __init__(self):
		rospy.init_node('controller',anonymous=True)
		
		rospy.Subscriber('pose',Pose,self.poseCallback)
		self.velPub = rospy.Publisher('cmd_vel',Twist,queue_size=1)
		self.rate = rospy.Rate(10)

		self.position = Point()

		# position reference
		self.reference = [1,1]
		# gain for P controller
		self.kp = rospy.get_param('/controller/gain', default='1.0')

	def poseCallback(self,msg):
		self.position = msg.position
		return 0

	def publishVel(self,vx,vy):
		vel_msg = Twist()
		vel_msg.linear.x = vx
		vel_msg.linear.y = vy
		self.velPub.publish(vel_msg)
		return 0

	def calcVelinput(self):
		# P controller
		vx = self.kp * (self.reference[0] - self.position.x)
		vy = self.kp * (self.reference[1] - self.position.y)
		return vx, vy
		
	def spin(self):
		rospy.sleep(2.)	
		

		rospy.loginfo("PRESS ENTER")
		raw_input()	
		rospy.loginfo("START")
		while not rospy.is_shutdown():
			vx, vy = self.calcVelinput()
			self.publishVel(vx, vy)
			self.rate.sleep()
		return 0

if __name__ == '__main__':
	try:
		controller = Controller()	
		controller.spin()
	except rospy.ROSInterruptException: pass
